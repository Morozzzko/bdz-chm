function [L, U] = LU(A)
    n = size(A, 1);
    L = eye([n n]);
    U = L;
    U(1, 1:n) = A(1, 1:n);
    L(2:n, 1) = A(2:n, 1)./U(1, 1);

    for i=2:n,
        for j = 1:n,
            S = 0;
            for k = 1:i-1,
                S = S + L(i, k) * U(k, j);
            end
            U(i, j) = A(i, j) - S;
        end

        
        for j = (i+1):n,
            S = 0;
            for k = 1:i-1,
                S = S + L(j, k) * U(k, i);
            end
            L(j, i) = 1/U(i,i) * (A(j, i) - S);
        end
    end
end
