function [X] = luinv(L, U)
    n = size(L, 2);
    X = zeros(n, n);
    for i=n:-1:1,
        for j=n:-1:1,
            S = 0;
            if (i == j)
                for p = (j+1):n,
                    S = S + U(j, p) * X(p, j);
                end
                
                X(i, j) = (1 - S)/U(j, j);
            elseif (i < j)
                for p = (i+1):n,
                    S = S + U(i, p) * X(p, j);
                end
                X(i, j) = -S/U(i, i);
            else
                for p = (j+1):n,
                    S = S + L(p, j) * X(i, p);
                end
                X(i, j) = -S;
            end
        end
    end
end