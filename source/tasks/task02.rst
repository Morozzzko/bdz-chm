=========
Задание 2
=========
 
Текст задачи
============
 
Написать последовательность инструкций Matlab, формирующих указанную матрицу. 

Около каждой инструкции указать промежуточный результат в виде матрицы. 

Разрешается использовать матричные функции (eye, repmat, flipud и др.). 

Использовать циклы нельзя.

**Входные данные**: Целое :math:`n \geq 20`

**Нужно получить**: 

.. math::

   \begin{pmatrix}
      1 & 2 & 3 & \cdots & n   \\
      2 & 1 & 2 & \cdots & n-1 \\
      3 & 2 & 1 & \cdots & n-2 \\
      \vdots  & \vdots  & \vdots & \ddots & \vdots  \\
      n & n-1 & n-2 & \cdots & 1
   \end{pmatrix}

Ход решения
===========

Matlab-код для решения данной задачи:

.. code-block:: matlab

   function [ A ] = F(n)
      A = repmat([1:n], n, 1);
      dA = A' - 1;
      A = A - dA;
      U = triu(A);
      L = fliplr(flipud(U));
      A = eye(N) + U + L;
   end
   
Результаты работы каждой функции
--------------------------------
 
 
A = repmat([1:n], n, 1)
***********************
Функция повторяет строку [1 2 ... n] n раз.
 
.. math::

   A = \begin{pmatrix}
      1 & 2 & 3 & \cdots & n \\
      \vdots & \vdots & \vdots &  & \vdots \\
      1 & 2 & 3 & \cdots & n
   \end{pmatrix}
   
dA = A' - 1;
************
Функция транспонирует матрицу A и из каждого элемента вычитает единицу.

Аналогичный результат можно достичь следующим способом:

.. code-block:: matlab

   dA = repmat([0:n-1]', 1, n);


.. math::

   dA = \begin{pmatrix}
      0 & \cdots & 0 \\
      1 & \cdots & 1 \\
      \vdots & & \vdots \\
      n-1 & \cdots & n-1
   \end{pmatrix}

A = A - dA
**********
Вычитаем из матрицы A матрицу dA и перезаписываем результат.

В результате получаем матрицу, на главной диагонали которой стоят нули, а вид матрицы выше главной диагонали именно тот, что нам нужен.

.. math::

   A = \begin{pmatrix}
      0 & 1 & 2 & \cdots & n \\
      -1 & 0 & 1 & \cdots & n-1 \\
      -2 & -1 & 0 & \cdots & n-2 \\
      \vdots & \vdots & \vdots &  & \vdots \\
      -n & -n + 1 & -n + 2 & \cdots & 1
   \end{pmatrix}

U = triu(A)
***********
Копирует из матрицу A в U элементы, стоящие выше главной диагонали

.. math::

   A = \begin{pmatrix}
      0 & 2 & \cdots & n \\
      \vdots & \ddots &  & \vdots \\
      \vdots &  & \ddots & 2 \\
      0 & \cdots & \cdots & 0
   \end{pmatrix}
   
L = fliplr(flipud(U))
*********************
Создает копию матрицы U, симметричную относительно главной диагонали

.. math::

   A = \begin{pmatrix}
      0 & \cdots & \cdots & 0 \\
      2 & \ddots &  & \vdots \\
      \vdots &  & \ddots & \vdots \\
      n & \cdots & 2 & 0
   \end{pmatrix}
   
A = eye(N) + U + L
******************
Складывает матрицы L, U и заполняет главную диагональ единицами (прибавив соразмерную единичную матрицу)


.. math::

   \begin{pmatrix}
      1 & 2 & 3 & \cdots & n   \\
      2 & 1 & 2 & \cdots & n-1 \\
      3 & 2 & 1 & \cdots & n-2 \\
      \vdots  & \vdots  & \vdots & \ddots & \vdots  \\
      n & n-1 & n-2 & \cdots & 1
   \end{pmatrix}