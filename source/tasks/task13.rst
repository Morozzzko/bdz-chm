==========
Задание 13
==========
 
Текст задачи
============
 
Правая часть СЛАУ Ax = f содержит погрешность, норма которой равна :math:`||f||_\infty`.

Оценить относительную погрешность нормы решения :math:`\frac{||\delta x||_\infty}{||x||_\infty}`.

Указание: воспользоваться оценкой :math:`\frac{||\delta x||_\infty}{||x||_\infty} \leq \mu (A) \frac{||\delta f||_\infty}{||f||_\infty}`.

.. math::

   A = \begin {pmatrix}
      6 & -10 & -5 \\
      -10 & -8 & -10 \\
      -1 & -3 & 6
   \end{pmatrix}, f = \begin {pmatrix}
      5.8 \\
      9.3 \\ 
      -9.2 
   \end{pmatrix}, || \delta f||_\infty = 0.3

Ход решения
===========

.. math:: 

   
   \mu(A) = ||A||_\infty \cdot ||A^{-1}||_\infty 
   
   A^{-1} \approx \begin{pmatrix}
       0.0610 &  -0.0587 &  -0.0469 \\
      -0.0548 &  -0.0243 &  -0.0861 \\
      -0.0172 &  -0.0219 &   0.1158
   \end{pmatrix}
   
   ||A||_\infty = \max_i(\sum\limits_{j=1}^{n} |a_{ij}|) = \max(21, 28, 9) = 28
   
   ||A^{-1}||_\infty \approx \max(0.1667, 0.1651, 0.1549) \approx 0.1667
   
   \mu(A) = 4.6676
   
   ||f||_\infty = \max(|5.8|, |9.3|, |-9.2|) = 9.3 
   
   \frac{||\delta x||_\infty}{||x||_\infty} \leq 4.6676 \frac{0.3}{9.3}
   
   \frac{||\delta x||_\infty}{||x||_\infty} \leq 0.1506
   
   
   

