==========
Задание 10
==========
 
Текст задачи
============
 
Получить (письменно) приближенное значение интеграла I по квадратурной формуле S(h) сначала с шагом :math:`h_1`, затем с шагом :math:`h_2`. 

Используя метод Рунге, указать насколько значение S(:math:`h_2`) отличается от истинного значения интеграла I.

.. math::

   I = \int_{-1}^3{\cos x \cosh x} dx 

:math:`S(h) = h\sum\limits_{i=1}^n f(x_i)` (формула правых прямоуг.) :math:`h_1 = 1`, :math:`h_2 = \frac{4}{10}`

Ход решения
===========


Вычисление
----------


С шагом :math:`h_1 = 1`
***********************

+---+-------------+----------------+-------------------------------------+
| i | :math:`x_i` | :math:`f(x_i)` | :math:`h\sum\limits_{j=1}^i f(x_j)` |
+===+=============+================+=====================================+
| 0 | -1.000000   | 0.833730       | 0.833730                            |
+---+-------------+----------------+-------------------------------------+
| 1 | 0.000000    | 1.000000       | 1.833730                            |
+---+-------------+----------------+-------------------------------------+
| 2 | 1.000000    | 0.833730       | 2.667460                            |
+---+-------------+----------------+-------------------------------------+
| 3 | 2.000000    | -1.565626      | 1.101834                            |
+---+-------------+----------------+-------------------------------------+
| 4 | 3.000000    | -9.966910      | -8.865076                           |
+---+-------------+----------------+-------------------------------------+

Приближенное значение интеграла: -8.865076

С шагом :math:`h_2 = 0.4`
*************************
+----+-------------+----------------+--------------------------------------+
| i  | :math:`x_i` | :math:`f(x_i)` | :math:`h \sum\limits_{j=1}^i f(x_j)` |
+====+=============+================+======================================+
| 0  | -1.000000   | 0.833730       | 0.333492                             |
+----+-------------+----------------+--------------------------------------+
| 1  | -0.600000   | 0.978407       | 0.724855                             |
+----+-------------+----------------+--------------------------------------+
| 2  | -0.200000   | 0.999733       | 1.124748                             |
+----+-------------+----------------+--------------------------------------+
| 3  | 0.200000    | 0.999733       | 1.524641                             |
+----+-------------+----------------+--------------------------------------+
| 4  | 0.600000    | 0.978407       | 1.916004                             |
+----+-------------+----------------+--------------------------------------+
| 5  | 1.000000    | 0.833730       | 2.249496                             |
+----+-------------+----------------+--------------------------------------+
| 6  | 1.400000    | 0.365582       | 2.395729                             |
+----+-------------+----------------+--------------------------------------+
| 7  | 1.800000    | -0.706024      | 2.113319                             |
+----+-------------+----------------+--------------------------------------+
| 8  | 2.200000    | -2.688219      | 1.038031                             |
+----+-------------+----------------+--------------------------------------+
| 9  | 2.600000    | -5.800285      | -1.282083                            |
+----+-------------+----------------+--------------------------------------+
| 10 | 3.000000    | -9.966910      | -5.268846                            |
+----+-------------+----------------+--------------------------------------+

Приближенное значение интеграла: -5.268846

Оценка погрешности
------------------

Применим формулу, описанную в лекциях

.. math::

   \left| I - S(h_2/2) \right| \approx \frac{\left|S(h_2) - S(h_2/2) \right|}{3} \leq \varepsilon

Выведем формулу для :math:`S(\frac{4h}{10})`.

.. math::
   
   \begin{cases}
      S(h) + ch^2 + O(h^3) = I \\
      S(\frac{4h}{10}) + c(\frac{4h}{10})^2 + O(h^3) = I
   \end{cases}
   
   S(h) - S(\frac{4h}{10}) - 0.84 ch^2 + O(h^3) = 0 \\
   ch^2 = \frac{S(h)-S(\frac{4h}{10})}{0.84}
   
   |I - S(\frac{4h}{10})| \approx |0.16 ch^2| \approx \frac{16}{48} |S(h) - S(\frac{4h}{10})| \approx 1.19874333333
   

По факту отклонение чуть больше и составляет 1.987146. Ошибка в вычислении появляется из-за того, что погрешность порядка :math:`h^3` вносит большой вклад при большом h.
