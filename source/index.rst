========================
БДЗ по Численным Методам
========================
**Выполнил**: Студент группы МП-22 Морозов Игорь

БДЗ-1
-----
.. toctree::
    :maxdepth: 2
    
    tasks/task01.rst
    tasks/task02.rst
    tasks/task03.rst
    tasks/task04.rst
    tasks/task05.rst
    tasks/task06.rst
    tasks/task07.rst
    tasks/task08.rst

БДЗ-2
-----
.. toctree::
    :maxdepth: 2
    
    tasks/task09.rst
    tasks/task10.rst
    tasks/task11.rst
    tasks/task12.rst
    tasks/task13.rst
    tasks/task14.rst
    tasks/task15.rst
    tasks/task16.rst
    