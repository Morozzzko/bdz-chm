clear
close all
clc

A = [1 2; 3 4];

det(A)

[L] = lu(A)

INV = inv(A)

U = L;
U(2,1) = 0;
L = [0 0; L(2, 1) 0];
U, L
LUINV = luinv(L, U)
INV